-- MySQL dump 10.13  Distrib 5.7.11, for osx10.11 (x86_64)
--
-- Host: localhost    Database: books
-- ------------------------------------------------------
-- Server version	5.7.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `knjige`
--

DROP TABLE IF EXISTS `knjige`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `knjige` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `avtor` varchar(255) NOT NULL DEFAULT '',
  `naziv` varchar(255) NOT NULL DEFAULT '',
  `opis` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `knjige`
--

LOCK TABLES `knjige` WRITE;
/*!40000 ALTER TABLE `knjige` DISABLE KEYS */;
INSERT INTO `knjige` VALUES (1,'J.R.R. Tolkien','Gospodar prstanov 1 - Bratovščina prstana','1. del trilogije Gospodar prstanov'),(2,'J.R.R. Tolkien','Gospodar prstanov 2 - Stolpa','2. del trilogije Gospodar prstanov'),(3,'J.R.R. Tolkien','Gospodar prstanov 3 - Kraljeva vrnitev','3. del trilogije Gospodar prstanov'),(4,'J.R.R. Tolkien','Hobit','Tja in spet nazaj'),(5,'J. K. Rowling','Harry Potter in kamen modrosti','Prva knjiga o Harryju Potterju.'),(6,'J. K. Rowling','Harry Potter in dvorana skrivnosti','Druga knjiga o Harryju Potterju.'),(7,'J. K. Rowling','Harry Potter in jetnik iz Azkabana','Tretja knjiga o Harryju Potterju.'),(8,'J. K. Rowling','Harry Potter in ognjeni kelih','Četrta knjiga o Harryju Potterju.'),(9,'J. K. Rowling','Harry Potter in feniksov red','Peta knjiga o Harryju Potterju.'),(10,'J. K. Rowling','Harry Potter in princ mešane krvi','Šesta knjiga o Harryju Potterju.'),(11,'J. K. Rowling','Harry Potter in svetinje smrti','Sedma knjiga o Harryju Potterju.');
/*!40000 ALTER TABLE `knjige` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `izposoje`
--

DROP TABLE IF EXISTS `izposoje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `izposoje` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_knjige` int(11) unsigned NOT NULL,
  `datum_izposoje` datetime DEFAULT NULL,
  `ime` varchar(255) NOT NULL DEFAULT '',
  `priimek` varchar(255) NOT NULL DEFAULT '',
  `nacrtovano_vracilo` datetime DEFAULT NULL,
  `dejansko_vracilo` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_knjige` (`id_knjige`),
  CONSTRAINT `izposoje_ibfk_1` FOREIGN KEY (`id_knjige`) REFERENCES `knjige` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `izposoje`
--

LOCK TABLES `izposoje` WRITE;
/*!40000 ALTER TABLE `izposoje` DISABLE KEYS */;
INSERT INTO `izposoje` VALUES (1,1,'2016-07-04 00:00:00','Janez','Novak','2016-07-18 00:00:00',NULL),(2,2,'2016-06-27 00:00:00','Janez','Novak','2016-07-04 00:00:00','2016-07-02 00:00:00');
/*!40000 ALTER TABLE `izposoje` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-07-04 15:49:52
