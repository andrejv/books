DROP TABLE IF EXISTS izposoje;
DROP TABLE IF EXISTS knjige;

CREATE TABLE knjige (
  id int(11) unsigned NOT NULL AUTO_INCREMENT,
  avtor varchar(255) NOT NULL DEFAULT '',
  naziv varchar(255) NOT NULL DEFAULT '',
  opis varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE izposoje (
  id int(11) unsigned NOT NULL AUTO_INCREMENT,
  id_knjige int(11) unsigned NOT NULL,
  datum_izposoje datetime DEFAULT NULL,
  ime varchar(255) NOT NULL DEFAULT '',
  priimek varchar(255) NOT NULL DEFAULT '',
  nacrtovano_vracilo datetime DEFAULT NULL,
  dejansko_vracilo datetime DEFAULT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (id_knjige) REFERENCES knjige(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO knjige VALUES
(1,'J.R.R. Tolkien','Gospodar prstanov 1 - Bratovščina prstana','1. del trilogije Gospodar prstanov'),
(2,'J.R.R. Tolkien','Gospodar prstanov 2 - Stolpa','2. del trilogije Gospodar prstanov'),
(3,'J.R.R. Tolkien','Gospodar prstanov 3 - Kraljeva vrnitev','3. del trilogije Gospodar prstanov'),
(4,'J.R.R. Tolkien','Hobit','Tja in spet nazaj'),
(5,'J. K. Rowling','Harry Potter in kamen modrosti','Prva knjiga o Harryju Potterju.'),
(6,'J. K. Rowling','Harry Potter in dvorana skrivnosti','Druga knjiga o Harryju Potterju.'),
(7,'J. K. Rowling','Harry Potter in jetnik iz Azkabana','Tretja knjiga o Harryju Potterju.'),
(8,'J. K. Rowling','Harry Potter in ognjeni kelih','Četrta knjiga o Harryju Potterju.'),
(9,'J. K. Rowling','Harry Potter in feniksov red','Peta knjiga o Harryju Potterju.'),
(10,'J. K. Rowling','Harry Potter in princ mešane krvi','Šesta knjiga o Harryju Potterju.'),
(11,'J. K. Rowling','Harry Potter in svetinje smrti','Sedma knjiga o Harryju Potterju.');

INSERT INTO izposoje VALUES
(1,1,'2016-07-04 00:00:00','Janez','Novak','2016-07-18 00:00:00',NULL),
(2,2,'2016-06-27 00:00:00','Janez','Novak','2016-07-04 00:00:00','2016-07-02 00:00:00');
