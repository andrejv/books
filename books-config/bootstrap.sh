#! /usr/bin/env bash

export DEBIAN_FRONTEND="noninteractive"

apt-get update
apt-get install -y apache2
apt-get install -y -q mysql-server

apt-add-repository -y ppa:openjdk-r/ppa
apt-get update
apt-get install -y openjdk-8-jdk

mysql -u root < /var/app/config/create.sql
mysql -u root books < /var/config/sql/books.sql

a2enmod proxy_http
cp -f /var/app/config/apache2.conf /etc/apache2/apache2.conf

service apache2 restart
