angular.module('starter.controllers', [])

    //.constant("server", "https://tst-andrejv.rhcloud.com/")
    .constant("server", "/")

    .controller('AppCtrl', function ($scope, $ionicModal, $timeout) {

    })

    // SEZNAM KNJIG
    .controller('KnjigeCtrl', function ($scope, $http, $ionicHistory, server) {

        $scope.knjige = [];
        $scope.spinnerHidden = false;
        $scope.message = "";

        $scope.refresh = function () {
            $ionicHistory.clearCache();
            $scope.spinnerHidden = false;
            $scope.message = "";
            $http.get(server + 'api/knjige').then(
                function (response) {
                    $scope.knjige = response.data;
                },
                function (data) {
                    $scope.message = "Povezava s strežnikom ni uspela!";
                }).finally(
                function () {
                    $scope.spinnerHidden = true;
                    // Stop the ion-refresher from spinning
                    $scope.$broadcast('scroll.refreshComplete');
                });
        };

        $scope.refresh();
    })

    // POGLED KNJIGA
    .controller('KnjigaCtrl', function ($scope, $http, $stateParams, $location, $ionicHistory, server) {
        $scope.id = $stateParams.id
        $scope.avtor = "";
        $scope.naziv = "";
        $scope.opis = "";
        $scope.izposoje = [];

        $http.get(server + 'api/knjiga/' + $scope.id).success(
            function (data) {
                $scope.avtor = data.avtor,
                    $scope.naziv = data.naziv,
                    $scope.opis = data.opis
            }
        );

        $http.get(server + 'api/knjiga/' + $scope.id + '/izposoje').success(
            function (data) {
                $scope.izposoje = data;
            }
        );

        $http.get(server + 'api/knjiga/' + $scope.id + '/status').success(
            function (data) {
                $scope.status = data.status;
            }
        );

        $scope.delete = function () {
            $ionicHistory.clearCache();
            $http.delete(server + 'api/knjiga/' + $scope.id).then(
                function (response) {
                    $location.url("/")
                }
            )
        };

        $scope.izposoja = function () {
            $location.url("/app/nova_izposoja/" + $scope.id)
        };

        $scope.uredi = function () {
            $location.url("/app/uredi_knjigo/" + $scope.id)
        };
    })

    // NOVA KNJIGA
    .controller('NovaKnjigaCtrl', function ($scope, $http, $location, $ionicHistory, server) {

        $scope.view = "Nova knjiga";
        $scope.fields = {
            avtor: "",
            naziv: "",
            opis: ""
        };

        $scope.submit = function () {

            var data = {
                avtor: $scope.fields.avtor,
                naziv: $scope.fields.naziv,
                opis: $scope.fields.opis
            };

            $ionicHistory.clearCache();

            $http.post(server + "api/nova_knjiga", data).then(
                function (response) {
                    var id = response.data.id;
                    if (id >= 0)
                        $location.url("/app/knjiga/" + id);
                    else
                        $location.url("/");
                })
        };

    })


    // UREDI KNJIGO
    .controller('UrediKnjigoCtrl', function ($scope, $http, $location, $stateParams, $ionicHistory, server) {

        $scope.msg = "";
        $scope.id = $stateParams.id;
        $scope.view = "Uredi knjigo";
        $scope.fields = {
            avtor: "",
            naziv: "",
            opis: ""
        };

        $http.get(server + "api/knjiga/" + $scope.id).success(
            function (response) {
                $scope.fields = response;
            }
        );

        $scope.submit = function () {

            var data = {
                avtor: $scope.fields.avtor,
                naziv: $scope.fields.naziv,
                opis: $scope.fields.opis
            };

            $ionicHistory.clearCache();

            $http.post(server + "api/knjiga/" + $scope.id, data).then(
                function (response) {
                    $location.url("/app/knjiga/" + $scope.id);
                })
        };
    })

    // SEZNAM IZPOSOJ
    .controller('IzposojeCtrl', function ($scope, $http, $ionicHistory, server) {

        $scope.izposoje = [];
        $scope.spinnerHidden = false;
        $scope.message = "";

        $scope.refresh = function () {
            $ionicHistory.clearCache();
            $scope.spinnerHidden = false;
            $scope.message = "";
            $http.get(server + 'api/izposoje').then(
                function (response) {
                    $scope.izposoje = response.data;
                },
                function (response) {
                    $scope.message = "Povezava s strežnikom ni uspela!";
                }).
                finally(
                function () {
                    $scope.spinnerHidden = true;
                    // Stop the ion-refresher from spinning
                    $scope.$broadcast('scroll.refreshComplete');
                }
                );
        };

        $scope.refresh();
    })


    // NOVA IZPOSOJA
    .controller('NovaIzposojaCtrl', function ($scope, $http, $stateParams, $location, $ionicHistory, server) {
        $scope.view = "Nova izposoja";
        $scope.fields = {
            ime: "",
            priimek: "",
            dtIz: new Date(),
            dtNa: new Date(),
            dyDe: ""
        }

        $scope.submit = function () {
            var data = {
                ime: $scope.fields.ime,
                priimek: $scope.fields.priimek,
                izposoja: $scope.fields.dtIz ? $scope.fields.dtIz.getTime() : "",
                nacrt: $scope.fields.dtNa ? $scope.fields.dtNa.getTime() : "",
                dejansko: $scope.fields.dtDe ? $scope.fields.dtDe.getTime() : ""

            };

            $ionicHistory.clearCache();

            $http.post(server + 'api/knjiga/' + $stateParams.id + '/izposoja', data).then(
                function (response) {
                    $location.url('/app/knjiga/' + $stateParams.id)
                },
                function (response) {
                    console.log("Error " + response);
                }
            )
        }
    })


    // UREDI IZPOSOJO
    .controller('UrediIzposojoCtrl', function ($scope, $http, $stateParams, $location, $ionicHistory, server) {
        $scope.view = "Uredi izposojo";
        $scope.fields = {
            ime: "",
            priimek: "",
            dtIz: new Date(),
            dtNa: new Date(),
            dyDe: ""
        }

        $http.get(server + 'api/izposoja/' + $stateParams.id).then(
            function (response) {
                $scope.fields.ime = response.data.ime;
                $scope.fields.priimek = response.data.priimek;
                $scope.fields.dtIz = new Date(parseInt(response.data.datum_izposoje));
                $scope.fields.dtNa = new Date(parseInt(response.data.nacrtovano_vracilo));
                $scope.fields.dtDe = new Date(parseInt(response.data.dejansko_vracilo));
            }
        )

        $scope.submit = function () {
            var data = {
                id: $stateParams.id,
                ime: $scope.fields.ime,
                priimek: $scope.fields.priimek,
                izposoja: $scope.fields.dtIz ? $scope.fields.dtIz.getTime() : "",
                nacrt: $scope.fields.dtNa ? $scope.fields.dtNa.getTime() : "",
                dejansko: $scope.fields.dtDe ? $scope.fields.dtDe.getTime() : ""
            };

            $ionicHistory.clearCache();

            $http.post(server + 'api/izposoja/' + $stateParams.id, data).then(
                function (response) {
                    $location.url('/app/izposoja/' + $stateParams.id)
                },
                function (response) {
                    console.log(response);
                }
            )
        }

        $scope.zbrisi = function () {
            $ionicHistory.clearCache();
            $http.delete(server + 'api/izposoja/' + $stateParams.id).then(
                function (response) {
                    $ionicHistory.goBack();
                }
            )
        }
    })


    // ISKANJE
    .controller('IsciCtrl', function ($scope, $http, server) {
        $scope.knjige = false;
        $scope.spinnerHidden = true;
        $scope.message = "";

        $scope.fields = {
            avtor: "",
            naziv: "",
            opis: ""
        };

        $scope.submit = function () {
            $scope.spinnerHidden = false;
            $scope.message = "";
            var data = {
                avtor: $scope.fields.avtor,
                naziv: $scope.fields.naziv,
                opis: $scope.fields.opis
            };

            $http.get(server + 'api/isci', { params: data }).then(
                function (response) {
                    if (response != [])
                        $scope.knjige = response.data;
                },
                function () {
                    $scope.message = "Povezava s strežnikom ni uspela!";
                }
            ).finally(
                function () {
                    $scope.spinnerHidden = true;
                }
                );
        };

        $scope.clear = function () {
            $scope.fields = {
                avtor: "",
                naziv: "",
                opis: ""
            };
            $scope.knjige = false;
            $scope.message = "";
        }

    })
