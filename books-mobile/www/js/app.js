// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers'])

    .run(function ($ionicPlatform) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });
    })

    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider

            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/menu.html',
                controller: 'AppCtrl'
            })

            .state('app.knjige', {
                url: '/knjige',
                views: {
                    menuContent: {
                        templateUrl: 'templates/knjige.html',
                        controller: 'KnjigeCtrl'
                    }
                }
            })

            .state('app.knjiga', {
                url: '/knjiga/:id',
                views: {
                    menuContent: {
                        templateUrl: 'templates/knjiga.html',
                        controller: 'KnjigaCtrl'
                    }
                }
            })

            .state('app.nova_knjiga', {
                url: '/nova_knjiga',
                views: {
                    menuContent: {
                        templateUrl: 'templates/nova.html',
                        controller: 'NovaKnjigaCtrl'
                    }
                }
            })

            .state('app.uredi_knjigo', {
                url: '/uredi_knjigo/:id',
                views: {
                    menuContent: {
                        templateUrl: 'templates/nova.html',
                        controller: 'UrediKnjigoCtrl'
                    }
                }
            })

            .state('app.isci', {
                url: '/isci',
                views: {
                    menuContent: {
                        templateUrl: 'templates/isci.html',
                        controller: 'IsciCtrl'
                    }
                }
            })


            .state('app.izposoje', {
                url: '/izposoje',
                views: {
                    menuContent: {
                        templateUrl: 'templates/izposoje.html',
                        controller: 'IzposojeCtrl'
                    }
                }
            })

            .state('app.nova_izposoja', {
                url: '/nova_izposoja/:id',
                views: {
                    menuContent: {
                        templateUrl: 'templates/izposoja.html',
                        controller: 'NovaIzposojaCtrl'
                    }
                }
            })

            .state('app.uredi_izposojo', {
                url: '/izposoja/:id',
                views: {
                    menuContent: {
                        templateUrl: 'templates/izposoja_uredi.html',
                        controller: 'UrediIzposojoCtrl'
                    }
                }
            })

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/knjige');
    });
