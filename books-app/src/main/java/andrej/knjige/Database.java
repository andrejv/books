package andrej.knjige;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Date;

import com.google.gson.Gson;
import com.mysql.cj.jdbc.MysqlDataSource;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Database {

	private String user;
	private String password;
	private MysqlDataSource ds;

	public Database() {
		user = System.getenv("OPENSHIFT_MYSQL_DB_USERNAME");
		password = System.getenv("OPENSHIFT_MYSQL_DB_PASSWORD");
		String url;

		if (user == null || password == null) {
			url = "jdbc:mysql://localhost:3306/books?serverTimezone=UTC&characterEncoding=UTF-8";
			user = "dev";
			password = "dev123";
		}
		else {
			String host = System.getenv("OPENSHIFT_MYSQL_DB_HOST");
			String port = System.getenv("OPENSHIFT_MYSQL_DB_PORT");
			url = "jdbc:mysql://" + host + ":" + port + "/books?serverTimezone=UTC&characterEncoding=UTF-8";
		}

		ds = new MysqlDataSource();
		ds.setURL(url);
	}

	public String getKnjige() {
		List<HashMap<String, String>> books = new ArrayList<HashMap<String, String>>();

		try (Connection conn = ds.getConnection(user, password)){
			Statement st = conn.createStatement();

			try (ResultSet rs = st.executeQuery("SELECT * FROM knjige")) {

				while (rs.next()) {
					String naziv = rs.getString("naziv");
					String avtor = rs.getString("avtor");
					String id = "" + rs.getInt("id");
                    String opis = rs.getString("opis");
					HashMap<String, String> mp = new HashMap<String, String>();
					mp.put("naziv", naziv);
					mp.put("avtor", avtor);
					mp.put("id", id);
					books.add(mp);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		Gson gson = new Gson();
		return gson.toJson(books);
	}

	public String addKnjiga(String avtor, String naziv, String opis) {
		HashMap<String, Integer> hm = new HashMap<String, Integer>();
		hm.put("id", -1);

		try (Connection conn = ds.getConnection(user, password)) {
			String query = "INSERT INTO knjige (avtor, naziv, opis) VALUES (?, ?, ?)";
			PreparedStatement stm = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			stm.setString(1, avtor);
			stm.setString(2, naziv);
			stm.setString(3, opis);
			stm.executeUpdate();

			ResultSet res = stm.getGeneratedKeys();
			if (res.next()) {
				hm.put("id", res.getInt(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		Gson gson = new Gson();
		return gson.toJson(hm);
	}

	public String getKnjiga(String id) {
		HashMap<String, String> mp = new HashMap<String, String>();

		try (Connection conn = ds.getConnection(user, password)){

			PreparedStatement stm = conn.prepareStatement("SELECT * FROM knjige WHERE id = ?");
			stm.setString(1, id);

			try (ResultSet res = stm.executeQuery()) {

				if (res.next()) {
					mp.put("avtor", res.getString("avtor"));
					mp.put("naziv", res.getString("naziv"));
					mp.put("opis", res.getString("opis"));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		Gson gson = new Gson();
		return gson.toJson(mp);
	}

	public String getIzposoje(String id) {
		List<HashMap<String, String>> lst = new ArrayList<HashMap<String, String>>();

		try (Connection conn = ds.getConnection(user, password)) {
			String query = "SELECT * FROM izposoje INNER JOIN knjige ON izposoje.id_knjige = knjige.id WHERE id_knjige = ?";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setString(1, id);

			try (ResultSet res = stm.executeQuery()) {

				while (res.next()) {
					HashMap<String, String> mp = new HashMap<String, String>();
					mp.put("ime", res.getString("ime"));
					mp.put("priimek", res.getString("priimek"));
					mp.put("datum", res.getString("datum_izposoje"));
					mp.put("id", res.getString("izposoje.id"));
					mp.put("naziv", res.getString("naziv"));
					mp.put("avtor", res.getString("avtor"));
					lst.add(mp);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		Gson gson = new Gson();
		return gson.toJson(lst);
	}

	public void addIzposoja(String ime, String priimek, String izposoja, String nacrt, String dejansko, String id) {
		try (Connection conn = ds.getConnection(user, password)) {
			String query = "INSERT INTO izposoje (ime, priimek, datum_izposoje, nacrtovano_vracilo, dejansko_vracilo, id_knjige) VALUES (?, ?, ?, ?, ?, ?)";
			PreparedStatement stm = conn.prepareStatement(query);
            if (dejansko == null) {
                dejansko = "";
            }
            stm.setString(1, ime);
			stm.setString(2, priimek);
			stm.setDate(3, izposoja.equals("") ? null : new Date(Long.parseLong(izposoja)));
			stm.setDate(4, nacrt.equals("") ? null : new Date(Long.parseLong(nacrt)));
			stm.setDate(5, dejansko.equals("") ? null : new Date(Long.parseLong(dejansko)));
			stm.setString(6, id);
			stm.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public String getIzposoja(String id) {
		HashMap<String, String> mp = new HashMap<String, String>();

		try (Connection conn = ds.getConnection(user, password)) {
			String query = "SELECT * FROM izposoje INNER JOIN knjige ON izposoje.id_knjige = knjige.id WHERE izposoje.id = ?";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setString(1, id);
			try (ResultSet res = stm.executeQuery()) {

				if (res.next()) {
					mp.put("id_knjige", res.getString("id_knjige"));
					mp.put("ime", res.getString("ime"));
					mp.put("priimek", res.getString("priimek"));
					mp.put("datum_izposoje", getTime(res.getDate("datum_izposoje")));
					mp.put("nacrtovano_vracilo", getTime(res.getDate("nacrtovano_vracilo")));
					mp.put("dejansko_vracilo", getTime(res.getDate("dejansko_vracilo")));
					mp.put("avtor", res.getString("avtor"));
					mp.put("naziv", res.getString("naziv"));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		Gson gson = new Gson();
		return gson.toJson(mp);
	}

	private String getTime(Date date) {
		return date!=null ? "" + date.getTime() : "";
	}

	public void updateIzposoja(String ime, String priimek, String izposoja, String nacrt, String dejansko, String id) {
        if (dejansko == null) {
            dejansko = "";
        }
		try (Connection conn = ds.getConnection(user, password)) {
			String query = "UPDATE izposoje SET ime = ?, priimek = ?, datum_izposoje = ?, nacrtovano_vracilo = ?, dejansko_vracilo = ? WHERE id = ?";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setString(1, ime);
			stm.setString(2, priimek);
			stm.setDate(3, izposoja.equals("") ? null : new Date(Long.parseLong(izposoja)));
			stm.setDate(4, nacrt.equals("") ? null : new Date(Long.parseLong(nacrt)));
			stm.setDate(5, dejansko.equals("") ? null : new Date(Long.parseLong(dejansko)));
			stm.setString(6, id);
			stm.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void updateKnjiga(String avtor, String naziv, String opis, String id) {
		try (Connection conn = ds.getConnection(user, password)) {
			PreparedStatement stm = conn.prepareStatement("UPDATE knjige SET avtor = ?, naziv = ?, opis = ? WHERE id = ?");
			stm.setString(1, avtor);
			stm.setString(2, naziv);
			stm.setString(3, opis);
			stm.setString(4, id);
			stm.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void deleteKnjiga(String id) {
		try (Connection conn = ds.getConnection(user, password)) {
			PreparedStatement stm = conn.prepareStatement("DELETE FROM izposoje WHERE id_knjige = ?");
			stm.setString(1, id);
			stm.executeUpdate();

			stm = conn.prepareStatement("DELETE FROM knjige WHERE id = ?");
			stm.setString(1, id);
			stm.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public String getIzposoje() {
		List<HashMap<String, String>> izposoje = new ArrayList<HashMap<String, String>>();

		try (Connection conn = ds.getConnection(user, password)) {
			Statement st = conn.createStatement();
			String query = "SELECT izposoje.id, izposoje.ime, izposoje.priimek, knjige.naziv FROM izposoje INNER JOIN knjige ON izposoje.id_knjige = knjige.id ORDER BY id DESC";
			try (ResultSet rs = st.executeQuery(query)) {

				while (rs.next()) {
					String ime = rs.getString("ime");
					String priimek = rs.getString("priimek");
					String id = "" + rs.getInt("id");
					String naziv = rs.getString("naziv");
					HashMap<String, String> mp = new HashMap<String, String>();
					mp.put("priimek", priimek);
					mp.put("ime", ime);
					mp.put("id", id);
					mp.put("naziv", naziv);
					izposoje.add(mp);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		Gson gson = new Gson();
		return gson.toJson(izposoje);
	}

	public String searchKnjige(String avtor, String naziv, String opis) {
		List<HashMap<String, String>> books = new ArrayList<HashMap<String, String>>();

		try (Connection conn = ds.getConnection(user, password)) {
			PreparedStatement st = conn.prepareStatement("SELECT * FROM knjige WHERE avtor LIKE ? AND naziv LIKE ? AND opis LIKE ?");
			st.setString(1, "%" + avtor + "%");
			st.setString(2, "%" + naziv + "%");
			st.setString(3, "%" + opis + "%");

			try (ResultSet rs = st.executeQuery()) {
				while (rs.next()) {
					String knaziv = rs.getString("naziv");
					String kavtor = rs.getString("avtor");
					String kid = "" + rs.getInt("id");
					HashMap<String, String> mp = new HashMap<String, String>();
					mp.put("naziv", knaziv);
					mp.put("avtor", kavtor);
					mp.put("id", kid);
					books.add(mp);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		Gson gson = new Gson();
		return gson.toJson(books);
	}

	public void deleteIzposoja(String id) {
		try (Connection conn = ds.getConnection(user, password)) {
			PreparedStatement stm = conn.prepareStatement("DELETE FROM izposoje WHERE id = ?");
			stm.setString(1, id);
			stm.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public boolean statusKnjige(String id) {
		boolean prosta = true;

		try (Connection conn = ds.getConnection(user, password)) {
			String query = "SELECT id FROM izposoje WHERE id_knjige = ? AND datum_izposoje < NOW() AND (dejansko_vracilo IS NULL OR dejansko_vracilo > NOW())";
			PreparedStatement stm = conn.prepareStatement(query);
			stm.setString(1, id);

			try (ResultSet rs = stm.executeQuery()) {
				if (rs.next()) {
					prosta = false;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return prosta;
	}
}
