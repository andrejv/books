package andrej.knjige;

import static spark.Spark.*;

import java.util.HashMap;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class KnjigeApp {

	private static final String IP_ADDRESS = System.getenv("OPENSHIFT_DIY_IP") != null ? System.getenv("OPENSHIFT_DIY_IP") : "localhost";
	private static final int PORT = System.getenv("OPENSHIFT_DIY_PORT") != null ? Integer.parseInt(System.getenv("OPENSHIFT_DIY_PORT")) : 4567;
	private static Logger log = Logger.getLogger(KnjigeApp.class.getName());

	public static void main(String[] args) {

		ipAddress(IP_ADDRESS);
		port(PORT);

		// PUBLIC FILES
		staticFileLocation("/public");

		before((req, res) -> {
			log.info("Accessing route: " + req.pathInfo());
		});

		// NOVA KNJIGA
		post("/api/nova_knjiga", (req, res) -> {
			HashMap<String, String> hm = toJson(req.body());

			String avtor = hm.get("avtor");
			String naziv = hm.get("naziv");
			String opis = hm.get("opis");

			Database db = new Database();

			res.header("Content-Type", "Application/json");
			return db.addKnjiga(avtor, naziv, opis);
		});

		// API ZA KNJIGO
		get("/api/knjiga/:id", (req, res) -> {
			Database db = new Database();
			res.header("Content-Type", "Application/json");
			return db.getKnjiga(req.params(":id"));
		});

		post("/api/knjiga/:id", (req, res) -> {
			Database db = new Database();
			HashMap<String, String> hm = toJson(req.body());

			String avtor = hm.get("avtor");
			String naziv = hm.get("naziv");
			String opis = hm.get("opis");

			db.updateKnjiga(avtor, naziv, opis, req.params(":id"));

			return "\"OK\"";
		});

		delete("/api/knjiga/:id", (req, res) -> {
			Database db = new Database();
			db.deleteKnjiga(req.params(":id"));

			res.header("Content-Type", "Application/json");
			return "\"OK\"";
		});

		get("/api/knjiga/:id/status", (req, res) -> {
			Database db = new Database();
			res.header("Content-Type", "Application/json");
			if (!db.statusKnjige(req.params(":id")))
				return "{\"status\":\"izposojena\"}";
			return "{\"status\":\"prosta\"}";
		});

		// SEZNAM IZPOSOJ ZA KNJIGO
		get("/api/knjiga/:id/izposoje", (req, res) -> {
			Database db = new Database();
			res.header("Content-Type", "Application/json");
			return db.getIzposoje(req.params(":id"));
		});

		// NOVA IZPOSOJA
		post("/api/knjiga/:id/izposoja", (req, res) -> {
			HashMap<String, String> hm = toJson(req.body());

			String ime = hm.get("ime");
			String priimek = hm.get("priimek");
			String izposoja = hm.get("datun_izposoje");
			String nacrt = hm.get("nacrtovano_vracilo");
			String dejansko = hm.get("dejansko_vracilo");

			Database db = new Database();
			db.addIzposoja(ime, priimek, izposoja, nacrt, dejansko, req.params(":id"));

			res.header("Content-Type", "Application/json");
			return "\"OK\"";
		});

		// API ZA IZPOSOJO
		get("/api/izposoja/:id",  (req, res) -> {
			Database db = new Database();

			return db.getIzposoja(req.params(":id"));
		});

		post("/api/izposoja/:id", (req, res) -> {
			HashMap<String, String> hm = toJson(req.body());

			String ime = hm.get("ime");
			String priimek = hm.get("priimek");
			String izposoja = hm.get("datum_izposoje");
			String nacrt = hm.get("nacrtovano_vracilo");
			String dejansko = hm.get("dejansko_vracilo");

			Database db = new Database();
			db.updateIzposoja(ime, priimek, izposoja, nacrt, dejansko, req.params(":id"));

			res.header("Content-Type", "Application/json");
			return "\"OK\"";
		});

		delete("/api/izposoja/:id", (req, res) -> {
			Database db = new Database();
			db.deleteIzposoja(req.params(":id"));
			res.header("Content-Type", "Application/json");
			return "\"OK\"";
		});

		// SEZNAM KNJIG
		get("/api/knjige", (req, res) -> {
			Database db = new Database();
			res.header("Content-Type", "Application/json");
			return db.getKnjige();
		});

		// SEZNAM IZPOSOJ
		get("/api/izposoje", (req, res) -> {
			Database db = new Database();
			res.header("Content-Type", "Application/json");
			return db.getIzposoje();
		});

		// ISKANJE
		get("/api/isci", (req, res) -> {
			Database db = new Database();

			String avtor = req.queryParams("avtor");
			String naziv = req.queryParams("naziv");
			String opis = req.queryParams("opis");

			if (avtor == null)
				avtor = "";
			if (naziv == null)
				naziv = "";
			if (opis == null)
				opis = "";

			res.header("Content-Type", "Application/json");
			return db.searchKnjige(avtor, naziv, opis);
		});
	}

	/**
	 * Spremeni JSON string v HashMap.
	 * @param str
	 * @return json kot HashMap
	 */
	private static HashMap<String, String> toJson(String str) {
		Gson gson = new Gson();
		HashMap<String, String> hm = gson.fromJson(str, new TypeToken<HashMap<String, String>>(){}.getType());

		return hm;
	}
}
