var app = angular.module('knjigeApp',[
  'ngRoute',
  'ui.bootstrap'
]);

app.config(function ($routeProvider) {
  $routeProvider.when('/', {
    templateUrl: 'views/seznam_knjig.html',
    controller: 'SeznamKnjigCtrl'
  })
  .when('/seznam_izposoj', {
    templateUrl: 'views/seznam_izposoj.html',
    controller: 'SeznamIzposojCtrl'
  })
  .when('/dodaj_knjigo', {
    templateUrl: 'views/knjiga_uredi.html',
    controller: 'NovaKnjigaCtrl'
  })
  .when('/knjiga/:knjigaId', {
    templateUrl: 'views/knjiga.html',
    controller: 'KnjigaCtrl'
  })
  .when('/knjiga/:knjigaId/edit', {
    templateUrl: 'views/knjiga_uredi.html',
    controller: 'KnjigaUrediCtrl'
  })
  .when('/knjiga/:knjigaId/izposoja', {
    templateUrl: 'views/izposoja.html',
    controller: 'IzposojaCtrl'
  })
  .when('/izposoja/:izposojaId', {
    templateUrl: 'views/izposoja.html',
    controller: 'IzposojaUrediCtrl'
  })
  .when('/isci', {
    templateUrl: 'views/isci.html',
    controller: 'IsciCtrl'
  })
  .otherwise({
    redirectTo: '/'
  })
});

app.controller('SeznamKnjigCtrl', function ($scope, $http) {
  $http.get('/api/knjige').success(function (data) {
    $scope.books = data;
  }).error(function (data, status) {
    console.log('Error ' + data)
  })
});

app.controller('SeznamIzposojCtrl', function ($scope, $http) {
  $http.get('/api/izposoje').success(function (data) {
    $scope.izposoje = data;
  }).error(function (data, status) {
    console.log('Error ' + data)
  })
});


app.controller('NovaKnjigaCtrl', function ($scope, $http, $location) {

  $scope.avtor = "";
  $scope.naziv = "";
  $scope.opis = "";
  $scope.preklici = "#/";
  $scope.stran = "Nova knjiga";

  $scope.submit = function() {

    var data = {
      avtor : $scope.avtor,
      naziv : $scope.naziv,
      opis :  $scope.opis
    };

    $http.post("/api/nova_knjiga", data).then(
      function (response) {
    	var id = response.data.id;
    	if (id>=0)
	  $location.url("/knjiga/" + id);
    	else
    	  $location.url("/");
      })
  };
});

app.controller('KnjigaCtrl', function ($scope, $routeParams, $http, $location) {

  $scope.id = $routeParams.knjigaId
  $scope.avtor = "";
  $scope.naziv = "";
  $scope.opis = "";
  $scope.izposoje = [];

  $http.get('/api/knjiga/' + $scope.id).success(
    function (data) {
      $scope.avtor = data.avtor,
      $scope.naziv = data.naziv,
      $scope.opis = data.opis
    }
  );

  $http.get('api/knjiga/' + $scope.id + '/izposoje').success(
    function (data) {
    	console.log(data);
      $scope.izposoje = data;
    }
  );

  $http.get('/api/knjiga/' + $scope.id + '/status').success(
    function (data) {
      $scope.status = data.status;
    }
  );

  $scope.delete = function () {
    $http.delete('/api/knjiga/' + $scope.id).then(
      function (response) {
	$location.url("/")
      }
    )
  };

});

app.controller('KnjigaUrediCtrl', function ($scope, $routeParams, $http, $location) {

  $scope.id = $routeParams.knjigaId
  $scope.preklici = "#/knjiga/" + $routeParams.knjigaId;
  $scope.stran = "Uredi knjigo";

  $http.get('/api/knjiga/' + $scope.id).success(
    function (data) {
      $scope.avtor = data.avtor,
      $scope.naziv = data.naziv,
      $scope.opis = data.opis
    }
  );

  $scope.submit = function () {
    var data = {
      avtor : $scope.avtor,
      naziv : $scope.naziv,
      opis : $scope.opis
    };

    $http.post('/api/knjiga/' + $scope.id, data).then(
      function (response) {
        $scope.msg = "Shranjeno!"
      },
      function (response) {
        $scope.msg = "Napaka pri shranjevanju!"
      }
    )
  };
});

app.controller('IzposojaCtrl', function ($scope, $routeParams, $http, $location) {

  $scope.submit = function() {
    var data = {
      ime : $scope.ime,
      priimek : $scope.priimek,
      datum_izposoje : $scope.dtIz ? $scope.dtIz.getTime() : "",
      nacrtovano_vracilo: $scope.dtNa ? $scope.dtNa.getTime() : "",
      dejansko_vracilo: $scope.dtDe ? $scope.dtDe.getTime() : ""
    };

    $http.post('/api/knjiga/' + $routeParams.knjigaId + '/izposoja', data).then(
      function (response) {
	$location.url('/knjiga/' + $routeParams.knjigaId)
      },
      function (response) {
	console.log("Error " + response);
      }
    )
  }

  $http.get('/api/knjiga/' + $routeParams.knjigaId).success(
    function (response) {
      $scope.avtor = response.avtor;
      $scope.naziv = response.naziv;
    });
  $scope.stran = "Nova izposoja";
  $scope.id = $routeParams.knjigaId;

  $scope.dtIz = new Date();
  $scope.dtNa = new Date();
  //$scope.dtDe = new Date();
  $scope.dateIz = {opened : false};
  $scope.dateNa = {opened : false};
  $scope.dateDe = {opened : false};
  $scope.openDate = function (id) {
    switch(id) {
      case 0:
        $scope.dateIz.opened = true;
        break;
      case 1:
        $scope.dateNa.opened = true;
        break;
      case 2:
        $scope.dateDe.opened = true;
        break;
    }
  }
});

app.controller('IzposojaUrediCtrl', function ($scope, $routeParams, $http, $location) {

  $scope.submit = function() {
    var data = {
      id : $routeParams.izposojaId,
      ime : $scope.ime,
      priimek : $scope.priimek,
      datum_izposoje : $scope.dtIz ? $scope.dtIz.getTime() : "",
      nacrtovano_vracilo: $scope.dtNa ? $scope.dtNa.getTime() : "",
      dejansko_vracilo: $scope.dtDe ? $scope.dtDe.getTime() : null
    };

    $http.post('/api/izposoja/' + $routeParams.izposojaId, data).then(
      function (response) {
	$scope.msg = "Shranjeno!"
      },
      function (response) {
    	$scope.msg = "Napaka pri shranjevanju!"
      }
    )
  }

  $scope.delete = function () {
    $http.delete('/api/izposoja/' + $routeParams.izposojaId).then(
      function (response) {
	$location.url("/")
      }
    )
  };

  $scope.stran = "Uredi izposojo";
  $http.get('/api/izposoja/' + $routeParams.izposojaId).success(
    function (data) {
      $scope.id = data.id_knjige;
      $scope.ime = data.ime;
      $scope.priimek = data.priimek;
      $scope.naziv = data.naziv;
      $scope.avtor = data.avtor;
      $scope.dtIz = new Date(parseInt(data.datum_izposoje));
      $scope.dtNa = new Date(parseInt(data.nacrtovano_vracilo));
      $scope.dtDe = new Date(parseInt(data.dejansko_vracilo));
    }
  );

  $scope.dateIz = {opened : false};
  $scope.dateNa = {opened : false};
  $scope.dateDe = {opened : false};
  $scope.openDate = function (id) {
    switch(id) {
      case 0:
        $scope.dateIz.opened = true;
        break;
      case 1:
        $scope.dateNa.opened = true;
        break;
      case 2:
        $scope.dateDe.opened = true;
        break;
    }
  }
});

app.controller('IsciCtrl', function ($scope, $routeParams, $http, $location) {
  $scope.books = false;

  $scope.submit = function () {
    var data = {
      avtor : $scope.avtor,
      naziv : $scope.naziv,
      opis : $scope.opis
    };
    $http.get('/api/isci', {params: data}).success(
      function (response) {
	if (response != [])
	  $scope.books = response;
      });
  };

  $scope.clear = function () {
    $scope.avtor = "";
    $scope.naziv = "";
    $scope.opis = "";
    $scope.books = false;
  }
});
